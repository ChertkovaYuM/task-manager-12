package ru.tsc.chertkova.tm.model;

import ru.tsc.chertkova.tm.enumerated.Status;

import java.util.UUID;

public class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public Status getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + " - " + name + " : " + description + ", " + status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

}

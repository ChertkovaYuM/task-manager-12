package ru.tsc.chertkova.tm.component;

import ru.tsc.chertkova.tm.api.controller.ICommandController;
import ru.tsc.chertkova.tm.api.controller.IProjectController;
import ru.tsc.chertkova.tm.api.controller.ITaskController;
import ru.tsc.chertkova.tm.api.repository.ICommandRepository;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.ICommandService;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.constant.ArgumentConst;
import ru.tsc.chertkova.tm.constant.TerminalConst;
import ru.tsc.chertkova.tm.controller.CommandController;
import ru.tsc.chertkova.tm.controller.ProjectController;
import ru.tsc.chertkova.tm.controller.TaskController;
import ru.tsc.chertkova.tm.repository.CommandRepository;
import ru.tsc.chertkova.tm.repository.ProjectRepository;
import ru.tsc.chertkova.tm.repository.TaskRepository;
import ru.tsc.chertkova.tm.service.CommandService;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.service.TaskService;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private void process(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showError(param);
        }
    }

    private void process(final String param) {
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                commandController.showError(param);
        }
    }

    public void initData() {
        taskService.create("task1", "vot");
        taskService.create("task2", "kak-to");
        taskService.create("task3", "tak");
        projectService.create("project1", "no");
        projectService.create("project2", "nikak");
        projectService.create("project3", "inache");
    }

    public void run(final String[] args) {
        commandController.showWelcome();
        process(args);
        initData();
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = TerminalUtil.nextLine();
            process(command);
            System.out.println();
        }
    }

    private static void exit() {
        System.exit(0);
    }

}
